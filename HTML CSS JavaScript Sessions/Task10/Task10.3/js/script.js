let arrayOfNo = prompt("Enter the comma separeted list of number: ").split(',').map(Number);

let onlyNo = arrayOfNo.filter(x => !(isNaN(x)));

let repeated = {};
for(let i=0;i<onlyNo.length;i++){
    if(repeated.hasOwnProperty(onlyNo[i])){
        repeated[onlyNo[i]] += 1;
    }else{
        repeated[onlyNo[i]]=1;
    }
}

for(no in repeated){
    if(repeated[no] >1){
        console.log(`the number ${no} appears ${repeated[no]-1} times more.`)
    }
}