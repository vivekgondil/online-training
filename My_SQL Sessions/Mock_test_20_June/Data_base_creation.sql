create schema book_depo;



create table authors(
	id int auto_increment,
    author_name varchar(30),
    about varchar(1000),
    created_on datetime not null,
    modified_on datetime,
    modified_by varchar(30) not null,
    primary key(id));
    
insert into authors (author_name, about, created_on,  modified_by)
values ("J. K. Rowling", "", '2020-5-20 10:01:00.999999', "Vivek Gondil");

create table publishers (
	id int auto_increment,
	publisher_name varchar(30),
	established_on varchar(4),
	addr varchar(1000),
	about varchar(1000),
    created_on datetime not null,
    modified_on datetime,
    modified_by varchar(30) not null,
    primary key(id)
);


insert into publishers (publisher_name, established_on, addr, about, created_on, modified_by)
values('HarperCollins Publishers India', '1992', 'Mumbai', 'They publish books in several segments such as Autobiography, Business, Biography, Fiction, Crime, Thriller, Mystery, Hindi, Poetry, Politics, Film & Music, Graphic Novel, History, Romance, Self-help, Sport, and Travel', '2020-5-20 10:01:00.999999', "Vivek Gondil"), 
('Penguin Random House India', '1985', 'It is the largest English language trade cum publisher in the country.', 'Banglore', '2020-5-20 10:01:00.999999', "Vivek Gondil");

create table genres (
	id int primary key auto_increment,
	genre varchar(50),
    created_on datetime not null,
    modified_on datetime,
    modified_by varchar(30) not null 
);
    
insert into genres (genre, created_on, modified_by )
values ('Autobiography' , '2020-5-20 10:01:00.999999', "Vivek Gondil"), ('Business', '2020-5-20 10:01:00.999999', "Vivek Gondil"), ('Biography', '2020-5-20 10:01:00.999999', "Vivek Gondil"), ('Fiction', '2020-5-20 10:01:00.999999', "Vivek Gondil"), ('Crime', '2020-5-20 10:01:00.999999', "Vivek Gondil"), ('Thriller', '2020-5-20 10:01:00.999999', "Vivek Gondil"), ('Mystery', '2020-5-20 10:01:00.999999', "Vivek Gondil"), ('History', '2020-5-20 10:01:00.999999', "Vivek Gondil"), ('Romance', '2020-5-20 10:01:00.999999', "Vivek Gondil"), ('Sport', '2020-5-20 10:01:00.999999', "Vivek Gondil");

create table books (
	id int auto_increment primary key,
    book_title varchar(255),
	book_author_id int not null,
	book_publisher_id int not null,
    genre_id int,
	year_of_release int,
	rating float,
    created_on datetime not null,
    modified_on datetime,
    modified_by varchar(30) not null ,
    foreign key (book_author_id) REFERENCES authors(id),
    foreign key (book_publisher_id) REFERENCES publishers(id),
    foreign key (genre_id) REFERENCES genres(id)
);


insert into books (book_title, book_author_id, book_publisher_id, genre_id, year_of_release, rating, created_on, modified_by)
values ("Harry Potter and Philosopher'ss stone", 1, 1, 1, '1997', 7.6, '2020-5-20 10:01:00.999999', "Vivek Gondil");

create table users (
	id int primary key auto_increment,
    first_name varchar(50),
    last_name varchar(50),
    gender enum('M','F'),
    dob date,
    user_stat enum('Normal', 'Premium'),
    addr varchar(1000),
    phone_no varchar(10),
    created_on datetime not null,
    modified_on datetime,
    modified_by varchar(30) not null 
);


insert into users(first_name, last_name, gender, dob, user_stat, addr, phone_no, created_on, modified_by)
values('Joe', 'Patil', 'M', '1997-12-18', 'Premium', 'Pune', '9876543210', '2020-5-20 10:01:00.999999', "Vivek Gondil"), ('Zina', 'Patil', 'F', '1997-12-18', 'Normal', 'Mumbai', '9876534210', '2020-5-20 10:01:00.999999', "Vivek Gondil");


create table rent_book (
	id int primary key auto_increment,
    book_id int not null references books(id),
    user_id int not null references users(id),
    created_on datetime not null,
    modified_on datetime,
    modified_by varchar(30) not null
);

insert into  rent_book(book_id, user_id, created_on, modified_by )
values(1,1, '2020-5-20 10:01:00.999999', "Vivek Gondil");


create table like_book (
	id int primary key auto_increment,
    book_id int not null references books(id),
    user_id int not null references users(id),
    created_on datetime not null
);


insert into  like_book (book_id, user_id, created_on )
values(1, 1, '2020-5-20 10:01:00.999999');


create table frined_list(
	id int primary key auto_increment,
    user_id_from int not null references users(id),
    user_id_to int not null references users(id),
    created_on datetime not null
);

insert into frined_list ( user_id_from, user_id_to, created_on)
values (1,1,'2020-5-20 10:01:00.999999');


create table wishlist(
	id int primary key auto_increment,
    book_id int not null references books(id),
    user_id int not null references users(id),
    created_on datetime not null
);

insert into wishlist(book_id, user_id, created_on)
values ( 1,1,'2020-7-20 10:00:00');

